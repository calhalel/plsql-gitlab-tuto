
/******************************************************************************/
/************************* PACKAGE DEFINITION *********************************/
/******************************************************************************/

CREATE OR REPLACE PACKAGE PKG_STG_SRC_ETL_TEMPLATE
AS
   /** =========================================================================
     Job..................: PKG_STG_SRC_ETL_TEMPLATE
     Author...............: Andrey.Marchenko
     Date.................: 07.11.2018

   ========================================================================= **/

   -- starts all steps
   PROCEDURE prc_job_start      (in_force_restart IN VARCHAR2 default 'N',
                                 in_full_load IN VARCHAR2 default 'N',
                                 in_load_dt_from IN DATE default TO_DATE('01.01.1111','DD.MM.YYYY'),
                                 in_load_dt_to IN DATE default TO_DATE('31.12.9999','DD.MM.YYYY'),
                                 in_load_nr_from IN NUMBER default 0,
                                 in_load_nr_to IN NUMBER default 0);

   PROCEDURE prc_job_init      (in_force_restart IN VARCHAR2 default 'N',
                                 in_full_load IN VARCHAR2 default 'N',
                                 in_load_dt_from IN DATE default TO_DATE('01.01.1111','DD.MM.YYYY'),
                                 in_load_dt_to IN DATE default TO_DATE('31.12.9999','DD.MM.YYYY'),
                                 in_load_nr_from IN NUMBER default 0,
                                 in_load_nr_to IN NUMBER default 0);

   PROCEDURE prc_job_deinit    (in_job_sid IN NUMBER default 0);

   -- extract
   PROCEDURE prc_job_step_001  (in_job_sid IN NUMBER default 0);
   -- extract
   PROCEDURE prc_job_step_002  (in_job_sid IN NUMBER default 0);
END;
/


/******************************************************************************/
/************************* PACKAGE BODY ***************************************/
/******************************************************************************/

CREATE OR REPLACE PACKAGE BODY PKG_STG_SRC_ETL_TEMPLATE AS
   -- Package Variables --
   gv_job_name      CONSTANT VARCHAR2(100) := 'PKG_STG_SRC_ETL_TEMPLATE';
   gv_step_name     VARCHAR2(4)            := 'N/A';
   gv_step_descr    VARCHAR2(4)            := 'N/A';
   gv_job_sid       NUMBER                 := -1;
   gv_sysdate       DATE                   := SYSDATE;
   -- Parameters for delta/full load
   gv_force_restart VARCHAR2(4)            := 'N';
   gv_full_load     VARCHAR2(4)            := 'N';
   gv_load_dt_from  DATE                   := TO_DATE('01.01.1111','DD.MM.YYYY');
   gv_load_dt_to    DATE                   := TO_DATE('31.12.9999','DD.MM.YYYY');
   gv_load_nr_from  NUMBER                 := 0;
   gv_load_nr_to    NUMBER                 := 0;
   -- Exit Exception
   e_prev_load      EXCEPTION;
   
/******************************************************************************/
PROCEDURE prc_job_start         (in_force_restart IN VARCHAR2 default 'N',
                                 in_full_load IN VARCHAR2 default 'N',
                                 in_load_dt_from IN DATE default TO_DATE('01.01.1111','DD.MM.YYYY'),
                                 in_load_dt_to IN DATE default TO_DATE('31.12.9999','DD.MM.YYYY'),
                                 in_load_nr_from IN NUMBER default 0,
                                 in_load_nr_to IN NUMBER default 0) AS
BEGIN

-- init
prc_job_init (in_force_restart => in_force_restart,
              in_full_load => in_full_load,
              in_load_dt_from => in_load_dt_from,
              in_load_dt_to => in_load_dt_to,
              in_load_nr_from => in_load_nr_from,
              in_load_nr_to => in_load_nr_to);

-- extract
prc_job_step_001 (gv_job_sid);
-- extract
prc_job_step_002 (gv_job_sid);
-- deinit
prc_job_deinit   (gv_job_sid);

END prc_job_start;


/******************************************************************************/
PROCEDURE prc_job_step_001     (in_job_sid IN NUMBER default 0) AS
  v_return NUMBER := 0;
  v_rows   NUMBER := 0;
  v_step_name CONSTANT VARCHAR2(100) := 'DB_SHEMA.TARGET_TABLE.FIRST_SELECT_INSERT';
BEGIN

  -- LOGIC START --

  --INSERT /*+ APPEND */ INTO
  SELECT 1 INTO v_rows FROM DUAL;

  v_rows := v_rows + sql%rowcount;

  COMMIT;
  -- LOGIC END --

  -- finish
  v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, v_step_name, ' Affected Rows: *** ' || v_rows || ' ***');

EXCEPTION
  WHEN others THEN
    v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, 'JOB_ERROR', v_step_name || ' Error: ' || SQLERRM);
    ROLLBACK; RAISE;
END prc_job_step_001;


/******************************************************************************/
PROCEDURE prc_job_step_002     (in_job_sid IN NUMBER default 0) AS
  v_return NUMBER := 0;
  v_rows   NUMBER := 0;
  v_step_name CONSTANT VARCHAR2(100) := 'DB_SHEMA.TARGET_TABLE.SECOND_SELECT_INSERT';
BEGIN

  -- LOGIC START --

  --INSERT /*+ APPEND */ INTO
  SELECT 1 INTO v_rows FROM DUAL;

  v_rows := v_rows + sql%rowcount;

  COMMIT;
  -- LOGIC END --
   
  -- finish
  v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, v_step_name, 'Affected Rows: *** ' || v_rows || ' ***');

EXCEPTION
  WHEN others THEN
    v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, 'JOB_ERROR', v_step_name || ' Error: ' || SQLERRM);
    ROLLBACK; RAISE;
END prc_job_step_002;


/******************************************************************************/
PROCEDURE prc_job_init       (in_force_restart IN VARCHAR2 default 'N',
                              in_full_load IN VARCHAR2 default 'N',
                              in_load_dt_from IN DATE default TO_DATE('01.01.1111','DD.MM.YYYY'),
                              in_load_dt_to IN DATE default TO_DATE('31.12.9999','DD.MM.YYYY'),
                              in_load_nr_from IN NUMBER default 0,
                              in_load_nr_to IN NUMBER default 0) AS
  v_return NUMBER := 0;
  v_step_name CONSTANT VARCHAR2(100) := 'JOB_INIT';
BEGIN
   -- initialize input variables
   IF (in_force_restart = 'Y' or in_force_restart = 'N') THEN gv_force_restart := in_force_restart; END IF;
   IF (in_full_load = 'Y' or in_full_load = 'N') THEN gv_full_load := in_full_load; END IF;
   IF (in_load_dt_from IS NOT NULL) THEN gv_load_dt_from := in_load_dt_from; END IF;
   IF (in_load_dt_to IS NOT NULL) THEN gv_load_dt_to := in_load_dt_to; END IF;
   IF (in_load_nr_from IS NOT NULL) THEN gv_load_nr_from := in_load_nr_from; END IF;
   IF (in_load_nr_to IS NOT NULL) THEN gv_load_nr_to := in_load_nr_to; END IF;
   -- get SID
   gv_job_sid := odiapp.pkg_utl_job_status.fnc_utl_new_sid(gv_job_name, gv_force_restart);
   -- job runnig is still running
   IF gv_job_sid = -1 THEN 
      RAISE e_prev_load;
   ELSE
   -- start
   v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, 'JOB_START', 
                                                  'SID:' || gv_job_sid || 
                                                  ' force_restart:' || gv_force_restart || ' full_load:' || gv_full_load || 
                                                  ' load_dt_from:' || gv_load_dt_from || ' load_dt_to:' || gv_load_dt_to ||
                                                  ' load_nr_from:' || gv_load_nr_from || ' load_nr_to:' || gv_load_nr_to);
   END IF;

EXCEPTION
  WHEN e_prev_load THEN
    DBMS_OUTPUT.PUT_LINE('Error: Previous Job is running');
    ROLLBACK; RAISE;
  WHEN others THEN
    v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, 'JOB_ERROR', v_step_name || ' Error: ' || SQLERRM);
    ROLLBACK; RAISE;
END prc_job_init;


/******************************************************************************/
PROCEDURE prc_job_deinit     (in_job_sid IN NUMBER default 0) AS
  v_return NUMBER := 0;
  v_step_name CONSTANT VARCHAR2(100) := 'JOB_DEINIT';
BEGIN
   -- finish
   v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, 'JOB_END', 'Finished.');
   
EXCEPTION
  WHEN others THEN
    v_return := odiapp.pkg_utl_job_status.fnc_utl_put_log(gv_job_sid, gv_job_name, 'JOB_ERROR', v_step_name || ' Error: ' || SQLERRM);
    ROLLBACK; RAISE;
END prc_job_deinit;


/******************************************************************************/
BEGIN
  DBMS_OUTPUT.ENABLE(4000);
  EXCEPTION
   WHEN OTHERS THEN
      -- Never stop
      DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
      NULL;

END PKG_STG_SRC_ETL_TEMPLATE;
/
